const express = require('express')
const app = express()
const port = 3000
var userRoutes = require('./routes/Users');
var benefitsRoutes = require('./routes/Benefits');
var calendarRoutes = require('./routes/Calendars');
var companyRoutes = require('./routes/Companies');
var documentRoutes = require('./routes/Documents');

app.use('/api/users', userRoutes);
app.use('/api/benefits', benefitsRoutes);
app.use('/api/calendars', calendarRoutes);
app.use('/api/companies', companyRoutes);
app.use('/api/coduments', documentRoutes);
    

app.listen(port, () => console.log(`Example app listening on port ${port}!`))